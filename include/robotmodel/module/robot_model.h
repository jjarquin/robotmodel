#ifndef ROBOT_MODEL_MODULE_H
#define ROBOT_MODEL_MODULE_H

#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/timer/timer.hpp>
#include <boost/bind.hpp>

#include "Eigen/Dense"

#include <alcommon/almodule.h>
#include <alproxies/almotionproxy.h>

#include "openhrc/core.h"
#include "openhrc/kinematics.h"
#include "openhrc/utils.h"

namespace AL{
class ALBroker;
class ALMotion;
}
class NaoRobot;

class RobotModel : public AL::ALModule{
public:
    RobotModel(boost::shared_ptr<AL::ALBroker> pBroker, const std::string name);
    virtual ~RobotModel();

private:

    void init();
    void initKinematics();  // load initial configuration and init kinematics
    void initJointPositionAndVelocityLimits();

    void printRobotStatus();

    void resetInternalModel(); //Set the inertial base pose to zero and read the configuration from robot.

    void updateModel();
    void updateEndEffectorsPose();
    void updateJointVelocityLimits();

    void updateRobot();
    hr::VectorXr integrateJointSpeed(const hr::VectorXr & q_hr, const hr::VectorXr & dq_hr);

    void subscribeDCMProcess();
    void subscribeALMProcess();
    void subscribeThreadedALMProcess();


    boost::shared_ptr<hr::core::MultiBody> robot; // Robot Multibody

    hr::VectorXr q_r; //robot configuration read from sensors.
    hr::VectorXr q_d; //robot configuration used to send position commands to the robot.

    int rightFootHandleId;
    int leftFootHandleId;

    int rightHandHandleId;
    int leftHandHandleId;

    int torsoHandleId;
    int headHandleId;

    hr::Vector3r rightFootPosition;
    hr::Vector3r rightFootOrientation;

    hr::Vector3r leftFootPosition;
    hr::Vector3r leftFootOrientation;

    hr::Vector3r rightHandPosition;
    hr::Vector3r rightHandOrientation;

    hr::Vector3r leftHandPosition;
    hr::Vector3r leftHandOrientation;

    hr::Vector3r torsoPosition;
    hr::Vector3r torsoOrientation;

    hr::Vector3r headPosition;
    hr::Vector3r headOrientation;

    hr::Vector3r comPosition;

    hr::SpatialVector rightFootPose;
    hr::SpatialVector leftFootPose;

    hr::SpatialVector rightHandPose;
    hr::SpatialVector leftHandPose;

    hr::SpatialVector torsoPose;
    hr::SpatialVector headPose;

    hr::SpatialVector comPose;

    hr::core::FootStatus footStatus;

    hr::real_t integrationStepSize;

    //! Time holder variable
    hr::real_t t;

    //! Lower velocity limits
    hr::VectorXr ld;

    //! Upper velocity limits
    hr::VectorXr ud;


    //! Lower position limits
    hr::VectorXr lb;

    //! Upper position limits
    hr::VectorXr ub;


    //! Lower Bounds for the Prioritized Inverse Kinematic Control
    hr::VectorXr lp;

    //! Upper Bounds for the Prioritized Inverse Kinematic Control
    hr::VectorXr up;

    NaoRobot * robotAPI;
    void dummyReadConfig();
    void dummySendConfig();
};

#endif //ROBOT_MODEL_MODULE_H
