/*
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */


#ifndef ROBOT_MODEL_INTERFACE_NAO_INTERFACE_H
#define ROBOT_MODEL_INTERFACE_NAO_INTERFACE_H

#include "robotmodel/interface/robot.h"
#include "robotmodel/interface/nao_robot.h"
#include "robotmodel/interface/nao_api.h"

#include "robotmodel/interface/dcm_api.h"
#include "robotmodel/interface/almotion_api.h"
#include "robotmodel/interface/threaded_almotion_api.h"


#endif // ROBOT_MODEL_INTERFACE_NAO_INTERFACE_H
