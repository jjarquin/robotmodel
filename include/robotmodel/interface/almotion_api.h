#ifndef ROBOT_MODEL_INTERFACE_ALMOTION_API_H
#define ROBOT_MODEL_INTERFACE_ALMOTION_API_H

#include "nao_api.h"

namespace AL{
class ALMotionProxy;
}

class ALMotionAPI: public NaoAPI{
public:
    ALMotionAPI(boost::shared_ptr<AL::ALBroker> pBroker);
    virtual void init();
    virtual hr::VectorXr readConfiguration();
    virtual void sendConfiguration(const hr::VectorXr & q);
    virtual hr::real_t getIntegrationStepSize() const { return kIntegrationStepSize;}


private:
    static const hr::real_t kIntegrationStepSize = 0.02;
    static const int kNAO_DOF = 26;

    void hrToAl(const hr::VectorXr & q_hr , hr::VectorXr & q_al);
    void alToHr(hr::VectorXr & q_hr , const hr::VectorXr & q_al);

    boost::shared_ptr<AL::ALMotionProxy> motionProxy;

};


#endif //ROBOT_MODEL_INTERFACE_ALMOTION_API_H
