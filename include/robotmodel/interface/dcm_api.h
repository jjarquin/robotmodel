#ifndef ROBOT_MODEL_INTERFACE_DCM_API_H
#define ROBOT_MODEL_INTERFACE_DCM_API_H

#include "nao_api.h"
#include <alvalue/alvalue.h>
namespace AL{
class DCMProxy;
class ALMemoryFastAccess;
}

class DCMAPI: public NaoAPI{
public:
    DCMAPI(boost::shared_ptr<AL::ALBroker> pBroker);
    virtual void init();
    virtual hr::VectorXr readConfiguration();
    virtual void sendConfiguration(const hr::VectorXr & q);
    virtual hr::real_t getIntegrationStepSize() const { return 0.0;}
private:
    static const hr::real_t kIntegrationStepSize = 0.01;
    static const int kNAO_DOF = 25;
    static const int kINERTIAL_SENSORS = 7;
    static const int kFSR_SENSORS = 6;

    boost::shared_ptr<AL::DCMProxy> dcmProxy;
    void initFastAccess();
    void createPositionActuatorAlias();
    void createHardnessActuatorAlias();
    void preparePositionActuatorCommand();
    void setStiffness(const float &stiffnessValue, const int & miliseconds);
    void alToHr(hr::VectorXr & q_hr , const hr::VectorXr & q_al);
    void hrToAl(const hr::VectorXr & q_hr , hr::VectorXr & q_al);

    // DCM timed commands
    AL::ALValue commands;
    // Sensors names
    std::vector<std::string> fSensorKeys;
    // Used for fast memory access
    boost::shared_ptr<AL::ALMemoryFastAccess> fMemoryFastAccess;

    // Store sensor values.
    std::vector<float> sensorValues;

    enum SensorType { kHEAD_PITCH,
                      kHEAD_YAW,
                      kL_ANKLE_PITCH,
                      kL_ANKLE_ROLL,
                      kL_ELBOW_ROLL,
                      kL_ELBOW_YAW,
                      kL_HAND,
                      kL_HIP_PITCH,
                      kL_HIP_ROLL,
                      kL_HIP_YAW_PITCH,
                      kL_KNEE_PITCH,
                      kL_SHOULDER_PITCH,
                      kL_SHOULDER_ROLL,
                      kL_WRIST_YAW,
                      kR_ANKLE_PITCH,
                      kR_ANKLE_ROLL,
                      kR_ELBOW_ROLL,
                      kR_ELBOW_YAW,
                      kR_HAND,
                      kR_HIP_PITCH,
                      kR_HIP_ROLL,
                      kR_KNEE_PITCH,
                      kR_SHOULDER_PITCH,
                      kR_SHOULDER_ROLL,
                      kR_WRIST_YAW,
                      kACC_X,
                      kACC_Y,
                      kACC_Z,
                      kGYR_X,
                      kGYR_Y,
                      kANGLE_X,
                      kANGLE_Y,
                      kL_COP_X,
                      kL_COP_Y,
                      kL_TOTAL_WEIGHT,
                      kR_COP_X,
                      kR_COP_Y,
                      kR_TOTAL_WEIGHT,
                      kHEAD_PITCH_S,
                      kHEAD_YAW_S,
                      kL_ANKLE_PITCH_S,
                      kL_ANKLE_ROLL_S,
                      kL_ELBOW_ROLL_S,
                      kL_ELBOW_YAW_S,
                      kL_HAND_S,
                      kL_HIP_PITCH_S,
                      kL_HIP_ROLL_S,
                      kL_HIP_YAW_PITCH_S,
                      kL_KNEE_PITCH_S,
                      kL_SHOULDER_PITCH_S,
                      kL_SHOULDER_ROLL_S,
                      kL_WRIST_YAW_S,
                      kR_ANKLE_PITCH_S,
                      kR_ANKLE_ROLL_S,
                      kR_ELBOW_ROLL_S,
                      kR_ELBOW_YAW_S,
                      kR_HAND_S,
                      kR_HIP_PITCH_S,
                      kR_HIP_ROLL_S,
                      kR_KNEE_PITCH_S,
                      kR_SHOULDER_PITCH_S,
                      kR_SHOULDER_ROLL_S,
                      kR_WRIST_YAW_S
                    };

};

#endif //ROBOT_MODEL_INTERFACE_DCM_API_H
