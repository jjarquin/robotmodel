#ifndef ROBOT_MODEL_INTERFACE_ROBOT_H
#define ROBOT_MODEL_INTERFACE_ROBOT_H
#include "openhrc/core.h"

class Robot{
public:
    virtual hr::VectorXr readConfiguration() = 0;
    virtual void sendConfiguration(const hr::VectorXr & q) = 0;
    virtual hr::real_t getIntegrationStepSize() const = 0;
    virtual ~Robot(){}
protected:
    hr::real_t integrationStepSize;
};

#endif //ROBOT_MODEL_INTERFACE_ROBOT_H
