#ifndef ROBOT_MODEL_INTERFACE_THREADED_ALMOTION_API_H
#define ROBOT_MODEL_INTERFACE_THREADED_ALMOTION_API_H

#include "nao_api.h"
#include "alproxies/almotionproxy.h"

class ThreadedALMotionAPI: public NaoAPI{
public:
    ThreadedALMotionAPI(boost::shared_ptr<AL::ALBroker> pBroker);
    virtual void init();
    virtual hr::VectorXr readConfiguration();
    virtual void sendConfiguration(const hr::VectorXr & q);
    virtual hr::real_t getIntegrationStepSize() const { return kIntegrationStepSize;}
private:
    static const hr::real_t kIntegrationStepSize = 0.02;
    boost::shared_ptr<AL::ALMotionProxy> motionProxy;
};

#endif //ROBOT_MODEL_INTERFACE_THREADED_ALMOTION_API_H
