#ifndef ROBOT_MODEL_INTERFACE_NAO_API_H
#define ROBOT_MODEL_INTERFACE_NAO_API_H

#include "openhrc/core.h"
namespace AL{
class ALBroker;
}
class NaoAPI{

public:
    NaoAPI(boost::shared_ptr<AL::ALBroker> pBroker);
    virtual void init() = 0;
    virtual hr::VectorXr readConfiguration() = 0;
    virtual void sendConfiguration(const hr::VectorXr & q) = 0;
    virtual hr::real_t getIntegrationStepSize() const = 0;
    virtual ~NaoAPI(){}

protected:
    hr::real_t integrationStepSize;
    boost::shared_ptr<AL::ALBroker> broker;
};



#endif //ROBOT_MODEL_INTERFACE_NAO_API_H
