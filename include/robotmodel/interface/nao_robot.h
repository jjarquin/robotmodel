#ifndef ROBOT_MODEL_INTERFACE_NAO_HRC_H
#define ROBOT_MODEL_INTERFACE_NAO_HRC_H

#include "nao_api.h"
#include "robot.h"

namespace AL{
class ALBroker;
}

class NaoRobot: public Robot{
public:
    NaoRobot(NaoAPI * api);
    virtual hr::VectorXr readConfiguration();
    virtual void sendConfiguration(const hr::VectorXr & q);
    virtual hr::real_t getIntegrationStepSize() const {return integrationStepSize;}
    virtual ~NaoRobot(){}
private:
    NaoAPI * communicationAPI;
};

#endif // NAO_HRC_H
