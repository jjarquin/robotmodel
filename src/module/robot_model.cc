#include "robotmodel/module/robot_model.h"
#include <alcommon/albroker.h>
#include <alcommon/alproxy.h>

#include "robotmodel/interface/nao_interface.h"

#ifdef  USE_LOCAL_XBOT //
std::string naoFile = "/home/airoa/Thesis_work/naoMove/svnOpenHRC/OpenHRC/src/data/Nao_blue_noninertial.xbot";
#else
std::string naoFile = "/home/nao/projects/hrData/Nao_blue_noninertial.xbot";
#endif

RobotModel::RobotModel(boost::shared_ptr<AL::ALBroker> pBroker, const std::string name): AL::ALModule(pBroker, name ){
    setModuleDescription( "RobotModel Module." );

    functionName("updateModel", getName() , "start");
    BIND_METHOD(RobotModel::updateModel);

    functionName("updateRobot", getName() , "start");
    BIND_METHOD(RobotModel::updateRobot);

    functionName("readConfig", getName() , "start");
    BIND_METHOD(RobotModel::dummyReadConfig);

    functionName("sendCofig", getName() , "start");
    BIND_METHOD(RobotModel::dummySendConfig);


    functionName("subscribeALMotion", getName() , "start");
    BIND_METHOD(RobotModel::subscribeALMProcess);

    functionName("subscribeThreadedALMotion", getName() , "start");
    BIND_METHOD(RobotModel::subscribeThreadedALMProcess);

    functionName("subscribeDCM", getName() , "start");
    BIND_METHOD(RobotModel::subscribeDCMProcess);


    functionName("resetInternalModel", getName() , "start");
    BIND_METHOD(RobotModel::resetInternalModel);

}
RobotModel::~RobotModel(){


}

void RobotModel::init(){
    //! Load multibody
    hr::core::World world;
    std::string sFile = naoFile;
    int robotId = world.getRobotsVector()->size();
    world.loadMultiBody(sFile,robotId, hr::core::kNAO);
    robot = world.getRobot(robotId);

    initKinematics();
    updateEndEffectorsPose();
    initJointPositionAndVelocityLimits();
    printRobotStatus();
}


void RobotModel::initKinematics(){

    q_r.setZero(30); //NAO_DOF + Virtual Bodies
    q_d.setZero(30);

    // TODO:Read robot configuration;
    q_d = q_r;

    //! Set robot configuration and create handles for every end effector.
    robot->setConfiguration(q_r);
    robot->computeForwardKinematics();

    hr::Vector3r rightFootHandle(0.0,0.0,-0.04);
    rightFootHandleId = robot->addOperationalHandle(hr::core::nao::kRightFoot,rightFootHandle);

    hr::Vector3r leftFootHandle(0.0,0.0,-0.04);
    leftFootHandleId = robot->addOperationalHandle(hr::core::nao::kLeftFoot,leftFootHandle);

    hr::Vector3r rightHandHandle(0.08,0.0,0.0);
    rightHandHandleId = robot->addOperationalHandle(hr::core::nao::kRightHand,rightHandHandle);

    hr::Vector3r leftHandHandle(0.08,0.0,0.0);
    leftHandHandleId = robot->addOperationalHandle(hr::core::nao::kLeftHand,leftHandHandle);

    hr::Vector3r torsoHandle(0.0,0.0,0.0);
    torsoHandleId = robot->addOperationalHandle(hr::core::nao::kTorso,torsoHandle);

    hr::Vector3r headHandle(0.0,0.0,0.0);
    headHandleId = robot->addOperationalHandle(hr::core::nao::kHead,headHandle);

    t = 0.0;

    integrationStepSize = 0.01; // TODO: integrationStepSize = interfaceGetIntegrationStepSize();

    footStatus = hr::core::kBothFeetOnGround;
}

void RobotModel::initJointPositionAndVelocityLimits(){

    hr::VectorXr lb_ = (robot->getJointLowLimits()-q_r)/integrationStepSize;
    hr::VectorXr ub_ = (robot->getJointUpLimits()-q_r)/integrationStepSize;

    lb.resize(29);
    ub.resize(29);

    lb.segment(0,24) = lb_.segment(0,24);
    lb.segment(24,5) = lb_.segment(25,5);

    ub.segment(0,24) = ub_.segment(0,24);
    ub.segment(24,5) = ub_.segment(25,5);

    ld.resize(29);
    ud.resize(29);

    hr::VectorXr ud_ = robot->getJointUpSpeedLimits();
    hr::VectorXr ld_ = robot->getJointLowSpeedLimits();

    ld.segment(0,24) = ld_.segment(0,24);
    ld.segment(24,5) = ld_.segment(25,5);

    ud.segment(0,24) = ud_.segment(0,24);
    ud.segment(24,5) = ud_.segment(25,5);

    lp = ld.cwiseMax(lb);
    up = ud.cwiseMin(ub);

}

void RobotModel::printRobotStatus(){
    std::cout << "Left Foot Pose: " << std::endl << leftFootPose << std::endl;
    std::cout << "Right Foot Pose: " << std::endl << rightFootPose << std::endl;
    std::cout << "Torso Pose: " << std::endl << torsoPose << std::endl;

    std::cout << "Left Hand Pose: " << std::endl << leftHandPose << std::endl;
    std::cout << "Right Hand Pose: " << std::endl << rightHandPose << std::endl;
    std::cout << "Head pose: " << std::endl << headPose << std::endl;

    std::cout << "CoM pose: " << std::endl << comPosition << std::endl;

    std::cout << "Robot configuration" << std::endl << ((180.0/(3.1415))*q_r.transpose()) << std::endl;
}

void RobotModel::resetInternalModel(){

    q_r.setZero(30); //NAO_DOF + Virtual Bodies
    q_d.setZero(30); //NAO_DOF + Virtual Bodies

    t = 0.0;
    footStatus = hr::core::kBothFeetOnGround;

    updateModel();
}



void RobotModel::updateModel(){

    //! update q_d values
    //if(updateValues){
    //  //update q_d with q_r
    //   updateValues = false;
    //}

    // Read sensor values
    //q_r = getNaoConfig(*motionProxy);
    q_r.segment(0,6) = q_d.segment(0,6);

    robot->setConfiguration(q_r);
    robot->computeForwardKinematics();

    updateEndEffectorsPose();
    updateJointVelocityLimits();
}



void RobotModel::updateEndEffectorsPose(){

    rightFootPosition = robot->getOperationalHandlePosition(hr::core::nao::kRightFoot,rightFootHandleId);
    rightFootOrientation = robot->getBodyOrientation(hr::core::nao::kRightFoot).eulerAngles(0,1,2);

    leftFootPosition = robot->getOperationalHandlePosition(hr::core::nao::kLeftFoot,leftFootHandleId);
    leftFootOrientation = robot->getBodyOrientation(hr::core::nao::kLeftFoot).eulerAngles(0,1,2);

    rightHandPosition = robot->getOperationalHandlePosition(hr::core::nao::kRightHand,rightHandHandleId);
    rightHandOrientation = robot->getBodyOrientation(hr::core::nao::kRightHand).eulerAngles(0,1,2);

    leftHandPosition = robot->getOperationalHandlePosition(hr::core::nao::kLeftHand,leftHandHandleId);
    leftHandOrientation = robot->getBodyOrientation(hr::core::nao::kLeftHand).eulerAngles(0,1,2);

    torsoPosition = robot->getOperationalHandlePosition(hr::core::nao::kTorso,torsoHandleId);
    torsoOrientation = robot->getBodyOrientation(hr::core::nao::kTorso).eulerAngles(0,1,2);

    headPosition = robot->getOperationalHandlePosition(hr::core::nao::kHead,headHandleId);
    headOrientation = robot->getBodyOrientation(hr::core::nao::kHead).eulerAngles(0,1,2);

    comPosition = robot->getCoM();

    rightFootPose << rightFootPosition , rightFootOrientation;
    leftFootPose << leftFootPosition, leftFootOrientation;

    rightHandPose << rightHandPosition , rightHandOrientation;
    leftHandPose << leftHandPosition , leftHandOrientation;

    torsoPose << torsoPosition, torsoOrientation;
    headPose << headPosition, headOrientation;
    comPose << comPosition , 0,0,0;
}

void RobotModel::updateJointVelocityLimits(){

    //Convert position limits to velocity limits using the First Euler Approximation
    hr::VectorXr lb_ = (robot->getJointLowLimits()-q_r)/integrationStepSize;
    hr::VectorXr ub_ = (robot->getJointUpLimits()-q_r)/integrationStepSize;

    lb.resize(29);
    ub.resize(29);

    lb.segment(0,24) = lb_.segment(0,24);
    lb.segment(24,5) = lb_.segment(25,5);

    ub.segment(0,24) = ub_.segment(0,24);
    ub.segment(24,5) = ub_.segment(25,5);

    lp = ld.cwiseMax(lb);
    up = ud.cwiseMin(ub);
}

void RobotModel::updateRobot(){

    hr::VectorXr dq;

    //! Integrate
    //integrateNaoSpeed(q_d,dq,integrationStepSize);
    t = t + integrationStepSize;

    //setNaoConfig(*motionProxy,q_d,integrationStepSize);

}

hr::VectorXr RobotModel::integrateJointSpeed(const hr::VectorXr & q_hr, const hr::VectorXr & dq_hr){
    hr::VectorXr q(30);
    hr::VectorXr qtmp(29);

    qtmp.segment(0,24) = q_hr.segment(0,24);
    qtmp.segment(24,5) = q_hr.segment(25,5);
    qtmp += dq_hr*integrationStepSize;

    q.segment(0,24) = qtmp.segment(0,24);
    q(24) = qtmp(18);
    q.segment(25,5) = qtmp.segment(24,5);

    return q;
}


void RobotModel::subscribeDCMProcess(){
    robotAPI = new NaoRobot( new DCMAPI(getParentBroker()) );
    hr::VectorXr q = robotAPI->readConfiguration();
    robotAPI->sendConfiguration(q);
}

void RobotModel::subscribeALMProcess(){
    robotAPI = new NaoRobot( new ALMotionAPI(getParentBroker()) );
    hr::VectorXr q = robotAPI->readConfiguration();
    robotAPI->sendConfiguration(q);
}

void RobotModel::dummyReadConfig(){
    q_r = robotAPI->readConfiguration();
    robot->setConfiguration(q_r);
    robot->computeForwardKinematics();

}
void RobotModel::dummySendConfig(){
    robotAPI->sendConfiguration(q_r);

}

void RobotModel::subscribeThreadedALMProcess(){

}


