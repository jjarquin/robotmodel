#include "robotmodel/interface/almotion_api.h"
#include <alcommon/albroker.h>
#include <alproxies/almotionproxy.h>
#include <alerror/alerror.h>



ALMotionAPI::ALMotionAPI(boost::shared_ptr<AL::ALBroker> pBroker): NaoAPI(pBroker){
    init();
}

void ALMotionAPI::init(){
    try
    {
        motionProxy = broker->getMotionProxy();

    }
    catch (AL::ALError& e)
    {
        throw ALERROR("ALMotionAPI", "init()", "Impossible to create ALMotion Proxy : " + e.toString());
    }
}

hr::VectorXr ALMotionAPI::readConfiguration(){
        const bool useSensors = true;
        const int kHRC_DOF = 30;


        AL::ALValue jointNames;
        jointNames = AL::ALValue::array("Body");
        std::vector<float> sensorAngles = motionProxy->getAngles(jointNames, useSensors);
        Eigen::Map<Eigen::VectorXf> q_al(sensorAngles.data(),kNAO_DOF);

        hr::VectorXr q(kHRC_DOF);
        alToHr(q, q_al.cast<hr::real_t>());
        return q;
}

void ALMotionAPI::sendConfiguration(const hr::VectorXr & q){

    AL::ALValue alAngles, timeLists;
    alAngles.clear();
    alAngles.arraySetSize(kNAO_DOF);

    timeLists.clear();
    timeLists.arraySetSize(kNAO_DOF);

    hr::VectorXr q_al(kNAO_DOF);

    hrToAl(q,q_al);


    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");
    for (int i = 0; i!= kNAO_DOF; i++) {
        timeLists[i] = AL::ALValue::array(static_cast<float>(kIntegrationStepSize));
        alAngles[i] = q_al(i);
    }
    motionProxy->setAngles(jointNames,alAngles,1.0);

}



void ALMotionAPI::hrToAl(const hr::VectorXr & q_hr , hr::VectorXr & q_al){
    //HRC Joints -> ALM Joints Mapping
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,25,26,27,28,29,30,14,15,16,17,18,-1};
    q_al.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_al(i) = q_hr(jointId[i]-1);
        }
    }

}

void ALMotionAPI::alToHr(hr::VectorXr & q_hr , const hr::VectorXr & q_al){
    //ALM Joints -> HRC Joints Mapping
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,-1,26,27,28,29,30,14,15,16,17,18,-1};
    q_hr.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_hr(jointId[i]-1) = q_al(i);
        }
    }
    //Nao  virutal HIP Joint
    q_hr(24) = q_hr(18);

}



