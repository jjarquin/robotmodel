#include "robotmodel/interface/nao_robot.h"

NaoRobot::NaoRobot(NaoAPI * api){
    communicationAPI = api;
}

hr::VectorXr NaoRobot::readConfiguration(){
    return communicationAPI->readConfiguration();
}

void NaoRobot::sendConfiguration(const hr::VectorXr & q){
    communicationAPI->sendConfiguration(q);
}
