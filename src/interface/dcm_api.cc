#include "robotmodel/interface/dcm_api.h"
#include <alcommon/albroker.h>
#include <alproxies/dcmproxy.h>
#include <almemoryfastaccess/almemoryfastaccess.h>
#include <alerror/alerror.h>

#include <iostream>
DCMAPI::DCMAPI(boost::shared_ptr<AL::ALBroker> pBroker): NaoAPI(pBroker),
    fMemoryFastAccess(boost::shared_ptr<AL::ALMemoryFastAccess>(new AL::ALMemoryFastAccess())){
    init();
}

void DCMAPI::init(){
    try
    {
        std::cout << "[DCMAPI] Create DCMProxy." << std::endl;
        dcmProxy = broker->getDcmProxy();
        std::cout << "[DCMAPI] Init FastAccess." << std::endl;
        initFastAccess();
        std::cout << "[DCMAPI] Create actuator alias." << std::endl;
        createPositionActuatorAlias();
        std::cout << "[DCMAPI] Create hardness alias." << std::endl;
        createHardnessActuatorAlias();
        std::cout << "[DCMAPI] Create position command." << std::endl;
        preparePositionActuatorCommand();
    }
    catch (AL::ALError& e)
    {
        throw ALERROR("DCMAPI", "init()", "Impossible to create DCM Proxy. Initialiation failed. " + e.toString());
    }
}

hr::VectorXr DCMAPI::readConfiguration(){
    const int kHRC_DOF = 30;
    //! Get all values from ALMemory using fastaccess
    fMemoryFastAccess->GetValues(sensorValues);

    //! Convert ALMemory to Eigen
    hr::VectorXr q(kHRC_DOF);
    Eigen::Map<Eigen::VectorXf> q_al(sensorValues.data(),kNAO_DOF);
    alToHr(q,q_al.cast<hr::real_t>());

    return q;
}

void DCMAPI::setStiffness(const float &stiffnessValue, const int & miliseconds){
    AL::ALValue stiffnessCommands;
    int DCMtime;
    try
    {
        DCMtime = dcmProxy->getTime(miliseconds);
    }
    catch (const AL::ALError &e)
    {
        throw ALERROR("DCMAPI" , "setStiffness()", "Error on DCM getTime : " + e.toString());
    }

    stiffnessCommands.arraySetSize(3);
    stiffnessCommands[0] = std::string("jointStiffness");
    stiffnessCommands[1] = std::string("Merge");
    stiffnessCommands[2].arraySetSize(1);
    stiffnessCommands[2][0].arraySetSize(2);
    stiffnessCommands[2][0][0] = stiffnessValue;
    stiffnessCommands[2][0][1] = DCMtime;
    try
    {
        dcmProxy->set(stiffnessCommands);
    }
    catch (const AL::ALError &e)
    {
        throw ALERROR("DCMAPI", "setStiffness()", "Error when sending stiffness to DCM : " + e.toString());
    }
}

void DCMAPI::sendConfiguration(const hr::VectorXr & q){
    int DCMtime;
    try
    {
        DCMtime = dcmProxy->getTime(0);
    }
    catch (const AL::ALError &e)
    {
        throw ALERROR("DCMAPI", "sendConfiguration()", "Error on DCM getTime : " + e.toString());
    }


    hr::VectorXr q_al(kNAO_DOF);
    Eigen::VectorXf q_alf(kNAO_DOF);
    hrToAl(q,q_al);
    q_alf = q_al.cast<float>();

    //! Update dcm actuator commands
    for (int i=0; i<25; i++)
    {
        commands[5][i][0] = q_alf(i);
    }
    commands[4][0] = DCMtime; //

    try
    {
        dcmProxy->setAlias(commands);
    }
    catch (const AL::ALError &e)
    {
        throw ALERROR("DCMAPI", "sendConfiguration()", "Error when sending command to DCM : " + e.toString());
    }


}



// ALMemory fast access
void DCMAPI::initFastAccess()
{
    fSensorKeys.clear();
    //  Here as an example inertial + joints + FSR are read
    fSensorKeys.resize(kNAO_DOF + kINERTIAL_SENSORS + kFSR_SENSORS);
    // Joints Sensor list
    fSensorKeys[kHEAD_PITCH]       = std::string("Device/SubDeviceList/HeadPitch/Position/Sensor/Value");
    fSensorKeys[kHEAD_YAW]         = std::string("Device/SubDeviceList/HeadYaw/Position/Sensor/Value");
    fSensorKeys[kL_ANKLE_PITCH]    = std::string("Device/SubDeviceList/LAnklePitch/Position/Sensor/Value");
    fSensorKeys[kL_ANKLE_ROLL]     = std::string("Device/SubDeviceList/LAnkleRoll/Position/Sensor/Value");
    fSensorKeys[kL_ELBOW_ROLL]     = std::string("Device/SubDeviceList/LElbowRoll/Position/Sensor/Value");
    fSensorKeys[kL_ELBOW_YAW]      = std::string("Device/SubDeviceList/LElbowYaw/Position/Sensor/Value");
    fSensorKeys[kL_HAND]           = std::string("Device/SubDeviceList/LHand/Position/Sensor/Value");
    fSensorKeys[kL_HIP_PITCH]      = std::string("Device/SubDeviceList/LHipPitch/Position/Sensor/Value");
    fSensorKeys[kL_HIP_ROLL]       = std::string("Device/SubDeviceList/LHipRoll/Position/Sensor/Value");
    fSensorKeys[kL_HIP_YAW_PITCH]  = std::string("Device/SubDeviceList/LHipYawPitch/Position/Sensor/Value");
    fSensorKeys[kL_KNEE_PITCH]     = std::string("Device/SubDeviceList/LKneePitch/Position/Sensor/Value");
    fSensorKeys[kL_SHOULDER_PITCH] = std::string("Device/SubDeviceList/LShoulderPitch/Position/Sensor/Value");
    fSensorKeys[kL_SHOULDER_ROLL]  = std::string("Device/SubDeviceList/LShoulderRoll/Position/Sensor/Value");
    fSensorKeys[kL_WRIST_YAW]      = std::string("Device/SubDeviceList/LWristYaw/Position/Sensor/Value");
    fSensorKeys[kR_ANKLE_PITCH]    = std::string("Device/SubDeviceList/RAnklePitch/Position/Sensor/Value");
    fSensorKeys[kR_ANKLE_ROLL]     = std::string("Device/SubDeviceList/RAnkleRoll/Position/Sensor/Value");
    fSensorKeys[kR_ELBOW_ROLL]     = std::string("Device/SubDeviceList/RElbowRoll/Position/Sensor/Value");
    fSensorKeys[kR_ELBOW_YAW]      = std::string("Device/SubDeviceList/RElbowYaw/Position/Sensor/Value");
    fSensorKeys[kR_HAND]           = std::string("Device/SubDeviceList/RHand/Position/Sensor/Value");
    fSensorKeys[kR_HIP_PITCH]      = std::string("Device/SubDeviceList/RHipPitch/Position/Sensor/Value");
    fSensorKeys[kR_HIP_ROLL]       = std::string("Device/SubDeviceList/RHipRoll/Position/Sensor/Value");
    fSensorKeys[kR_KNEE_PITCH]     = std::string("Device/SubDeviceList/RKneePitch/Position/Sensor/Value");
    fSensorKeys[kR_SHOULDER_PITCH] = std::string("Device/SubDeviceList/RShoulderPitch/Position/Sensor/Value");
    fSensorKeys[kR_SHOULDER_ROLL]  = std::string("Device/SubDeviceList/RShoulderRoll/Position/Sensor/Value");
    fSensorKeys[kR_WRIST_YAW]      = std::string("Device/SubDeviceList/RWristYaw/Position/Sensor/Value");

    // Inertial sensors
    fSensorKeys[kACC_X]   = std::string("Device/SubDeviceList/InertialSensor/AccX/Sensor/Value");
    fSensorKeys[kACC_Y]   = std::string("Device/SubDeviceList/InertialSensor/AccY/Sensor/Value");
    fSensorKeys[kACC_Z]   = std::string("Device/SubDeviceList/InertialSensor/AccZ/Sensor/Value");
    fSensorKeys[kGYR_X]   = std::string("Device/SubDeviceList/InertialSensor/GyrX/Sensor/Value");
    fSensorKeys[kGYR_Y]   = std::string("Device/SubDeviceList/InertialSensor/GyrY/Sensor/Value");
    fSensorKeys[kANGLE_X] = std::string("Device/SubDeviceList/InertialSensor/AngleX/Sensor/Value");
    fSensorKeys[kANGLE_Y] = std::string("Device/SubDeviceList/InertialSensor/AngleY/Sensor/Value");

    // Some FSR sensors
    fSensorKeys[kL_COP_X]        = std::string("Device/SubDeviceList/LFoot/FSR/CenterOfPressure/X/Sensor/Value");
    fSensorKeys[kL_COP_Y]        = std::string("Device/SubDeviceList/LFoot/FSR/CenterOfPressure/Y/Sensor/Value");
    fSensorKeys[kL_TOTAL_WEIGHT] = std::string("Device/SubDeviceList/LFoot/FSR/TotalWeight/Sensor/Value");
    fSensorKeys[kR_COP_X]        = std::string("Device/SubDeviceList/RFoot/FSR/CenterOfPressure/X/Sensor/Value");
    fSensorKeys[kR_COP_Y]        = std::string("Device/SubDeviceList/RFoot/FSR/CenterOfPressure/Y/Sensor/Value");
    fSensorKeys[kR_TOTAL_WEIGHT] = std::string("Device/SubDeviceList/RFoot/FSR/TotalWeight/Sensor/Value");

    // Create the fast memory access
    fMemoryFastAccess->ConnectToVariables(broker, fSensorKeys, false);
}

void DCMAPI::createPositionActuatorAlias()
{
    std::cout << "Create position actuator alias" << std::endl;

    AL::ALValue jointAliasses;

    jointAliasses.arraySetSize(2);
    jointAliasses[0] = std::string("jointActuator"); // Alias for all 25 joint actuators
    jointAliasses[1].arraySetSize(kNAO_DOF);

    // Joints actuator list

    jointAliasses[1][kHEAD_PITCH]       = std::string("Device/SubDeviceList/HeadPitch/Position/Actuator/Value");
    jointAliasses[1][kHEAD_YAW]         = std::string("Device/SubDeviceList/HeadYaw/Position/Actuator/Value");
    jointAliasses[1][kL_ANKLE_PITCH]    = std::string("Device/SubDeviceList/LAnklePitch/Position/Actuator/Value");
    jointAliasses[1][kL_ANKLE_ROLL]     = std::string("Device/SubDeviceList/LAnkleRoll/Position/Actuator/Value");
    jointAliasses[1][kL_ELBOW_ROLL]     = std::string("Device/SubDeviceList/LElbowRoll/Position/Actuator/Value");
    jointAliasses[1][kL_ELBOW_YAW]      = std::string("Device/SubDeviceList/LElbowYaw/Position/Actuator/Value");
    jointAliasses[1][kL_HAND]           = std::string("Device/SubDeviceList/LHand/Position/Actuator/Value");
    jointAliasses[1][kL_HIP_PITCH]      = std::string("Device/SubDeviceList/LHipPitch/Position/Actuator/Value");
    jointAliasses[1][kL_HIP_ROLL]       = std::string("Device/SubDeviceList/LHipRoll/Position/Actuator/Value");
    jointAliasses[1][kL_HIP_YAW_PITCH]  = std::string("Device/SubDeviceList/LHipYawPitch/Position/Actuator/Value");
    jointAliasses[1][kL_KNEE_PITCH]     = std::string("Device/SubDeviceList/LKneePitch/Position/Actuator/Value");
    jointAliasses[1][kL_SHOULDER_PITCH] = std::string("Device/SubDeviceList/LShoulderPitch/Position/Actuator/Value");
    jointAliasses[1][kL_SHOULDER_ROLL]  = std::string("Device/SubDeviceList/LShoulderRoll/Position/Actuator/Value");
    jointAliasses[1][kL_WRIST_YAW]      = std::string("Device/SubDeviceList/LWristYaw/Position/Actuator/Value");
    jointAliasses[1][kR_ANKLE_PITCH]    = std::string("Device/SubDeviceList/RAnklePitch/Position/Actuator/Value");
    jointAliasses[1][kR_ANKLE_ROLL]     = std::string("Device/SubDeviceList/RAnkleRoll/Position/Actuator/Value");
    jointAliasses[1][kR_ELBOW_ROLL]     = std::string("Device/SubDeviceList/RElbowRoll/Position/Actuator/Value");
    jointAliasses[1][kR_ELBOW_YAW]      = std::string("Device/SubDeviceList/RElbowYaw/Position/Actuator/Value");
    jointAliasses[1][kR_HAND]           = std::string("Device/SubDeviceList/RHand/Position/Actuator/Value");
    jointAliasses[1][kR_HIP_PITCH]      = std::string("Device/SubDeviceList/RHipPitch/Position/Actuator/Value");
    jointAliasses[1][kR_HIP_ROLL]       = std::string("Device/SubDeviceList/RHipRoll/Position/Actuator/Value");
    jointAliasses[1][kR_KNEE_PITCH]     = std::string("Device/SubDeviceList/RKneePitch/Position/Actuator/Value");
    jointAliasses[1][kR_SHOULDER_PITCH] = std::string("Device/SubDeviceList/RShoulderPitch/Position/Actuator/Value");
    jointAliasses[1][kR_SHOULDER_ROLL]  = std::string("Device/SubDeviceList/RShoulderRoll/Position/Actuator/Value");
    jointAliasses[1][kR_WRIST_YAW]      = std::string("Device/SubDeviceList/RWristYaw/Position/Actuator/Value");

    // Create alias
    try
    {
        dcmProxy->createAlias(jointAliasses);
    }
    catch (const AL::ALError &e)
    {
        throw ALERROR("DCMAPI", "createPositionActuatorAlias()", "Error when creating Alias : " + e.toString());
    }
}

void DCMAPI::createHardnessActuatorAlias()
{
    std::cout << "Create hardness actuator alias" << std::endl;

    AL::ALValue jointAliasses;
    // Alias for all joint stiffness
    jointAliasses.clear();
    jointAliasses.arraySetSize(2);
    jointAliasses[0] = std::string("jointStiffness"); // Alias for all 25 actuators
    jointAliasses[1].arraySetSize(kNAO_DOF);

    // stiffness list
    jointAliasses[1][kHEAD_PITCH]        = std::string("Device/SubDeviceList/HeadPitch/Hardness/Actuator/Value");
    jointAliasses[1][kHEAD_YAW]          = std::string("Device/SubDeviceList/HeadYaw/Hardness/Actuator/Value");
    jointAliasses[1][kL_ANKLE_PITCH]     = std::string("Device/SubDeviceList/LAnklePitch/Hardness/Actuator/Value");
    jointAliasses[1][kL_ANKLE_ROLL]      = std::string("Device/SubDeviceList/LAnkleRoll/Hardness/Actuator/Value");
    jointAliasses[1][kL_ELBOW_ROLL]      = std::string("Device/SubDeviceList/LElbowRoll/Hardness/Actuator/Value");
    jointAliasses[1][kL_ELBOW_YAW]       = std::string("Device/SubDeviceList/LElbowYaw/Hardness/Actuator/Value");
    jointAliasses[1][kL_HAND]            = std::string("Device/SubDeviceList/LHand/Hardness/Actuator/Value");
    jointAliasses[1][kL_HIP_PITCH]       = std::string("Device/SubDeviceList/LHipPitch/Hardness/Actuator/Value");
    jointAliasses[1][kL_HIP_ROLL]        = std::string("Device/SubDeviceList/LHipRoll/Hardness/Actuator/Value");
    jointAliasses[1][kL_HIP_YAW_PITCH]   = std::string("Device/SubDeviceList/LHipYawPitch/Hardness/Actuator/Value");
    jointAliasses[1][kL_KNEE_PITCH]      = std::string("Device/SubDeviceList/LKneePitch/Hardness/Actuator/Value");
    jointAliasses[1][kL_SHOULDER_PITCH]  = std::string("Device/SubDeviceList/LShoulderPitch/Hardness/Actuator/Value");
    jointAliasses[1][kL_SHOULDER_ROLL]   = std::string("Device/SubDeviceList/LShoulderRoll/Hardness/Actuator/Value");
    jointAliasses[1][kL_WRIST_YAW]       = std::string("Device/SubDeviceList/LWristYaw/Hardness/Actuator/Value");
    jointAliasses[1][kR_ANKLE_PITCH]     = std::string("Device/SubDeviceList/RAnklePitch/Hardness/Actuator/Value");
    jointAliasses[1][kR_ANKLE_ROLL]      = std::string("Device/SubDeviceList/RAnkleRoll/Hardness/Actuator/Value");
    jointAliasses[1][kR_ELBOW_ROLL]      = std::string("Device/SubDeviceList/RElbowRoll/Hardness/Actuator/Value");
    jointAliasses[1][kR_ELBOW_YAW]       = std::string("Device/SubDeviceList/RElbowYaw/Hardness/Actuator/Value");
    jointAliasses[1][kR_HAND]            = std::string("Device/SubDeviceList/RHand/Hardness/Actuator/Value");
    jointAliasses[1][kR_HIP_PITCH]       = std::string("Device/SubDeviceList/RHipPitch/Hardness/Actuator/Value");
    jointAliasses[1][kR_HIP_ROLL]        = std::string("Device/SubDeviceList/RHipRoll/Hardness/Actuator/Value");
    jointAliasses[1][kR_KNEE_PITCH]      = std::string("Device/SubDeviceList/RKneePitch/Hardness/Actuator/Value");
    jointAliasses[1][kR_SHOULDER_PITCH]  = std::string("Device/SubDeviceList/RShoulderPitch/Hardness/Actuator/Value");
    jointAliasses[1][kR_SHOULDER_ROLL]   = std::string("Device/SubDeviceList/RShoulderRoll/Hardness/Actuator/Value");
    jointAliasses[1][kR_WRIST_YAW]       = std::string("Device/SubDeviceList/RWristYaw/Hardness/Actuator/Value");

    // Create alias
    try
    {
        dcmProxy->createAlias(jointAliasses);
    }
    catch (const AL::ALError &e)
    {
        throw ALERROR("DCMAPI", "createHardnessActuatorAlias()", "Error when creating Alias : " + e.toString());
    }
}


void DCMAPI::preparePositionActuatorCommand()
{
    commands.arraySetSize(6);
    commands[0] = std::string("jointActuator");
    commands[1] = std::string("ClearAll"); // Erase all previous commands
    commands[2] = std::string("time-separate");
    commands[3] = 0;

    commands[4].arraySetSize(1);
    //commands[4][0]  Will be the new time
    commands[5].arraySetSize(kNAO_DOF); // For all joints

    for (int i=0; i<25; i++)
    {
        commands[5][i].arraySetSize(1);
        //commands[5][i][0] will be the new angle
    }
}



void DCMAPI::alToHr(hr::VectorXr &q_hr, const hr::VectorXr &q_al){
    int jointId[kNAO_DOF] = {8,7,23,24,12,11,-1,21,20,19,22,9,10,13,29,30,17,16,-1,27,26,28,14,15,18};
    //q_hr.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_hr(jointId[i]-1) = q_al(i);
        }
    }
    q_hr(24) = q_hr(18);
}

void DCMAPI::hrToAl(const hr::VectorXr &q_hr, hr::VectorXr &q_al){
    int jointId[kNAO_DOF] = {8,7,23,24,12,11,-1,21,20,19,22,9,10,13,29,30,17,16,-1,27,26,28,14,15,18};
    q_al.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_al(i) = q_hr(jointId[i]-1);
        }
    }

    //Do not move hands
    q_al(6) = std::numeric_limits<float>::quiet_NaN();
    q_al(18) = std::numeric_limits<float>::quiet_NaN();
}
