if( NOT (CMAKE_SYSTEM_PROCESSOR STREQUAL "i486"))
#linux and apple configuration
    #Finding lib chrono
    clean(BOOST_CHRONO)
    flib(BOOST_CHRONO boost_chrono)
    export_lib(BOOST_CHRONO)

    #Finding lib chrono
    clean(BOOST_TIMER)
    flib(BOOST_TIMER boost_timer)
    export_lib(BOOST_TIMER)

else( NOT (CMAKE_SYSTEM_PROCESSOR STREQUAL "i486"))
#atom configuration
   find_package(boost_chrono)
   find_package(boost_timer)
endif( NOT (CMAKE_SYSTEM_PROCESSOR STREQUAL "i486"))