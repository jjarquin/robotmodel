RobotModel module for the nao robot.

RobotModel is a LocalModule for the humanoid robot Nao. This module provides a common API for the Nao ALMotion and DCM Api.

It is currently in the development phase.

Todo: Build instructions
Configuration
Dependencies
Install

If you are interested in contributing to the project contact: Julio Jarquin - jjarquin.ct@gmail.com